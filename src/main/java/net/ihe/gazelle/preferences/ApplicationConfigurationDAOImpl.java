package net.ihe.gazelle.preferences;

import net.ihe.gazelle.preferences.exception.EntityConstraintException;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Map;

@Singleton
@AutoCreate
@Name("applicationConfigurationDAO")
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class ApplicationConfigurationDAOImpl implements ApplicationConfigurationDAO {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationConfigurationDAOImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    // Cache to store values instead of reading each time the psql database
    private DB mapdb;
    private Map<String, String> cache;

    public ApplicationConfigurationDAOImpl() {
        // init cache
        mapdb = DBMaker.newMemoryDB().make();
        cache = mapdb.getHashMap("applicationConfigurationCache");
    }

    @PreDestroy
    public void destroy() {
        mapdb.close();
    }

    @Override
    public List<ApplicationConfiguration> getAll() {
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery(entityManager);
        query.variable().order(true);
        return query.getList();
    }

    @Override
    public String getValue(String variable) {
        synchronized (cache) {
            if (!cache.containsKey(variable)) {
                ApplicationConfiguration appConfig = getByVariable(variable);
                if (appConfig != null) {
                    if (appConfig.getValue() == null || appConfig.getValue().isEmpty()) {
                        LOG.warn("Preference {} may not be initialized", appConfig.getVariable());
                    }
                    cache.put(appConfig.getVariable(), appConfig.getValue());
                } else {
                    LOG.error("Preference {} is not defined", variable);
                    return null;
                }
            }
            return cache.get(variable);
        }
    }

    @Override
    public ApplicationConfiguration getByVariable(String variable) {
        ApplicationConfigurationQuery query = new ApplicationConfigurationQuery(entityManager);
        query.variable().eq(variable);
        ApplicationConfiguration appConfig = query.getUniqueResult();
        return appConfig;
    }

    @Override
    public ApplicationConfiguration save(ApplicationConfiguration applicationConfiguration) throws EntityConstraintException {
        try {
            synchronized (this) {
                applicationConfiguration = entityManager.merge(applicationConfiguration);
                entityManager.flush();
                resetCache();
            }
        } catch (PersistenceException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                throw new EntityConstraintException((ConstraintViolationException) e.getCause());
            } else {
                throw e;
            }
        }
        return applicationConfiguration;
    }

    @Override
    public void resetCache() {
        cache.clear();
    }

}

package net.ihe.gazelle.preferences;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Abderrazek Boufahja > INRIA Rennes IHE development Project
 */
@Entity
@Table(name = "app_configuration", schema = "public", uniqueConstraints = {
        @UniqueConstraint(name = "uk_app_configuration_variable", columnNames = {"variable"})})
@SequenceGenerator(name = "app_configuration_sequence", sequenceName = "app_configuration_id_seq", allocationSize = 1)
public class ApplicationConfiguration implements Serializable {

    private static final long serialVersionUID = 678209368462007952L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_configuration_sequence")
    private Integer id;

    @Column(name = "variable", unique = true, nullable = false)
    private String variable;

    @Column(name = "value")
    private String value;

    // constructors ////////////////////////////////////////////////////////////////////////////

    public ApplicationConfiguration() {
    }

    public ApplicationConfiguration(String variable, String value) {
        this.variable = variable;
        this.value = value;
    }

    // getters and setters //////////////////////////////////////////////////////////////////////

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVariable() {
        return this.variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // hashcode and equals //////////////////////////////////////////////////////////////////////

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.value == null) ? 0 : this.value.hashCode());
        result = (prime * result) + ((this.variable == null) ? 0 : this.variable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        ApplicationConfiguration other = (ApplicationConfiguration) obj;
        if (this.value == null) {
            if (other.value != null) {
                return false;
            }
        } else if (!this.value.equals(other.value)) {
            return false;
        }
        if (this.variable == null) {
            if (other.variable != null) {
                return false;
            }
        } else if (!this.variable.equals(other.variable)) {
            return false;
        }
        return true;
    }

}

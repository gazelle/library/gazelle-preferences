package net.ihe.gazelle.preferences;

import net.ihe.gazelle.services.GenericServiceLoader;

import java.util.Date;


public class PreferenceService {

	private PreferenceService() {
		super();
	}

	private static PreferenceProvider getProvider() {
		return GenericServiceLoader.getService(PreferenceProvider.class);
	}

	public static String getString(String key) {
		return getProvider().getString(key);
	}

	public static Integer getInteger(String key) {
		return getProvider().getInteger(key);
	}

	public static Object getObject(String key) {
		return getProvider().getObject(key);
	}

	public static Boolean getBoolean(String key) {
		return getProvider().getBoolean(key);
	}

	public static void setObject(Object key, Object value) {
		getProvider().setObject(key, value);
	}

	public static void setString(String key, String value) {
		getProvider().setString(key, value);
	}

	public static void setInteger(String key, Integer value) {
		getProvider().setInteger(key, value);
	}

	public static void setBoolean(String key, Boolean value) {
		getProvider().setBoolean(key, value);
	}

	public static void setDate(String key, Date value) {
		getProvider().setDate(key, value);
	}

}

package net.ihe.gazelle.preferences;

import net.ihe.gazelle.preferences.exception.EntityConstraintException;

import javax.ejb.Local;
import java.util.List;

@Local
public interface ApplicationConfigurationDAO {

    List<ApplicationConfiguration> getAll();

    String getValue(String variable);

    ApplicationConfiguration getByVariable(String variable);

    ApplicationConfiguration save(ApplicationConfiguration applicationConfiguration) throws EntityConstraintException;

    void resetCache();
}

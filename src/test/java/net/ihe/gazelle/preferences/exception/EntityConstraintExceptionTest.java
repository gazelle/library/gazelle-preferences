package net.ihe.gazelle.preferences.exception;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.SQLException;

@RunWith(MockitoJUnitRunner.class)
public class EntityConstraintExceptionTest {

    private static final String UNIQUE_ERROR_MESSAGE = "ERROR: duplicate key value violates unique constraint \"uk_ohkbj3qngl9vkufma34rkn81u\"\n" +
            "  Détail : Key (keyword)=(test) already exists.";
    private static final String UNIQUE_HUMAN_MESSAGE = "keyword with value 'test' already exists";

    @Mock
    private ConstraintViolationException constraintViolationException;
    @Mock
    private SQLException sqlException;


    @Test
    public void uniqueConstraintViolationMessageTest() {

        Mockito.when(sqlException.getMessage()).thenReturn(UNIQUE_ERROR_MESSAGE);
        Mockito.when(constraintViolationException.getSQLException()).thenReturn(sqlException);

        EntityConstraintException entityConstraintException = new EntityConstraintException(constraintViolationException);
        Assert.assertEquals("The entityConstraintException should return the message '" + UNIQUE_HUMAN_MESSAGE + "'", UNIQUE_HUMAN_MESSAGE,
                entityConstraintException.getMessage());

    }

}
